<%@ include file="frags/headers.jsp" %>
	<title>SWIMv2 | Suggestions</title>
</head>
<%@ include file="frags/top.jsp" %>
<%@ include file="frags/initialContent.jsp" %>
<% /*AFTER THIS LINE BEGINS THE CONTENT OF THE PAGE*/ %>
<%
RegisteredUser otheruser = search.searchByUsername(request.getParameter("otheruser")).iterator().next();
List<RegisteredUser> suggestions = friendman.suggestFriends(loggeduser.getId(), otheruser.getId());
%>
<% 
if (suggestions.isEmpty()){%>
	<p class="noResults">Sorry, it seems there are no other people you may know among <%=otheruser.getUsername()%>'s friends that you're not already friend with.</p>	
<%} else {%>
	<p class="noResults">Here's some people u may know among <%=otheruser.getUsername()%>'s friends.</p>
	<div class="results">
	<%
	int i=0;
	for(RegisteredUser u: suggestions) {
		Set<Ability> skills = userman.getSkills(u.getId());
			if(i%2 == 0){%>
				<div class="resultObj clearfix white">
			<%}else{%>
				<div class="resultObj clearfix gray">
			<%}%>
				<div class="searchUserPicture"><img src="<%= u.getPicture()%>"/></div>
				<div class="searchUserDataContent">
					<div class="searchUserData">
						<div class="searchUserDataRow"><span class="fieldName">Username: </span><span class="fieldData"><%= u.getUsername()%></span></div>
						<div class="searchUserDataRow"><span class="fieldName">First Name: </span><span class="fieldData"><%= u.getFirstname()%></span></div>
						<div class="searchUserDataRow"><span class="fieldName">Last Name: </span><span class="fieldData"><%= u.getLastname()%></span></div>				
						<div class="searchUserDataRow"><span class="fieldName">E-mail: </span><span class="fieldData"><%= u.getEmail()%></span></div>
					</div>
					<%if(!skills.isEmpty()){%>
					<div class="searchUserSkills">
						<span class="fieldName">Abilities:</span>
						<ul>						
						<%												
							for(Ability a:skills) {
								out.print("<li>");
								out.print(a.getName());
								out.print("</li>");
							}
						%>
						</ul>
					</div>
					<%}%>				
				</div>				
					<% if(friendman.isFriend(loggeduser.getId(), u.getId())) {%>
					<form class="sendFriendshipRequest" method="post" action="#">
						<input class="sendFriendshipRequestButton" value="Amico" type="button">
					</form>
					<%} else if (friendman.isFriendshipRequested(loggeduser.getId(), u.getId())) { %>						
					<button class="waitingFriendshipRequest">Richiesta di Amicizia in Sospeso</button>								
					<%} else { %>
					<form class="sendFriendshipRequest" method="post" action="./SendRequestServlet">
						<input class="sendSuggestFriendshipRequestButton" value="+1 Add Friend" type="button">
						<input type="hidden" name="sender" value="<%= loggeduser.getId()%>">
						<input type="hidden" name="receiver" value="<%= u.getId()%>">
						<input type="hidden" name="receiverName" value="<%= u.getUsername()%>">
						<input type="hidden" name="requestType" value="friendship">
						<input type="hidden" name="suggested" value="suggested">
					</form>
					<%} %>											
				</div>
		<%i++;%>	
	<%}%>
		</div>
<%}%>
<%@ include file="frags/footer.jsp" %>