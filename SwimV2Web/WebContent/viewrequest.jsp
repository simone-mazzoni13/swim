<%@ include file="frags/headers.jsp" %>
	<title>SWIMv2 | Request</title>
</head>
<%@ include file="frags/top.jsp" %>
<%@ include file="frags/initialContent.jsp" %>
<% /*AFTER THIS LINE BEGINS THE CONTENT OF THE PAGE*/ %>
<div id="viewRequestPage">
<% 
if (request.getParameter("requestType").equals("help")) {
	HelpRequest req = reqman.viewHelpRequest(Long.parseLong(request.getParameter("id_request")));
	%>
	<h1>Help request from <span class="viewRequestUsername"><%= req.getSender().getUsername()%></span> (<%= req.getDate().toString().substring(0,10)%>)</h1>
	<div class="viewRequestContent">
		<p class="username"><%= req.getSender().getUsername()%> says...</p>
		<div class="viewRequestMessage"><%=req.getMessage()%></div>
		<form method="post" action="./SendResponseServlet">
			<input type="hidden" name="responseType" value="help">
			<input type="hidden" name="id_request" value=<%= req.getId() %>>
			<textarea name="message">Write a reply...</textarea>
			<div style="text-align:center;margin-bottom:10px;">
				<input class="accept" type="submit" name="result" value="Accept">
				<input class="decline" type="submit" name="result" value="Decline">
			</div>
		</form>
	</div>		
	<%
} else if (request.getParameter("requestType").equals("friendship")) {
	FriendshipRequest req = reqman.viewFriendshipRequest(Long.parseLong(request.getParameter("id_request")));
	%>
	<h1>Friendship request from <span class="viewRequestUsername"><%= req.getSender().getUsername()%></span> (<%= req.getDate().toString().substring(0,10)%>)</h1>
	<div class="viewRequestContent">
		<div class="viewRequestMessage"><%=req.getMessage()%></div>
		<form method="post" action="./SendResponseServlet">
			<input type="hidden" name="responseType" value="friendship">
			<input type="hidden" name="id_request" value=<%= req.getId() %>>
			<textarea name="message">Write a reply...</textarea>
			<div style="text-align:center;margin-bottom:10px;">
				<input class="accept" type="submit" name="result" value="Accept">
				<input class="decline" type="submit" name="result" value="Decline">
			</div>
		</form>
	</div>	
	<%	
} else if (request.getParameter("requestType").equals("ability")) {
	AbilityRequest req = reqman.viewAbilityRequest(Long.parseLong(request.getParameter("id_request")));
	%>
	<h1>Ability request from <span class="viewRequestUsername"><%= req.getSender().getUsername()%></span> (<%= req.getDate().toString().substring(0,10)%>)</h1>
	<div class="viewRequestContent">
		<div class="viewRequestMessage"><%=req.getMessage()%></div>
		<form method="post" action="./SendResponseServlet">
			<input type="hidden" name="responseType" value="ability">
			<input type="hidden" name="id_request" value=<%= req.getId() %>>
			<input type="hidden" name="id_admin" value=<%= loggedadmin.getId() %>>
			<textarea name="message">Write a reply...</textarea>
			<div style="text-align:center;margin-bottom:10px;">
				<input class="accept" type="submit" name="result" value="Accept">
				<input class="decline" type="submit" name="result" value="Decline">
			</div>
		</form>
	</div>	
	<%
}
%>
</div>
<%@ include file="frags/footer.jsp" %>