<%@page import="java.util.*,swim.entitybeans.*,swim.sessionbeans.*,javax.naming.*"%>
<%
RegisteredUser loggeduser =  (RegisteredUser)request.getSession().getAttribute("loggeduser");
Admin loggedadmin =  (Admin)request.getSession().getAttribute("loggedadmin");
InitialContext jndiContext = new InitialContext();
AbilityManagerRemote abilman = (AbilityManagerRemote) jndiContext.lookup("AbilityManager/remote");
RequestRemote reqman = (RequestRemote) jndiContext.lookup("Request/remote");
ResponseRemote repman = (ResponseRemote) jndiContext.lookup("Response/remote");
UserManagerRemote userman = (UserManagerRemote) jndiContext.lookup("UserManager/remote");
FriendshipManagerRemote friendman = (FriendshipManagerRemote) jndiContext.lookup("FriendshipManager/remote");
SearchRemote search = (SearchRemote) jndiContext.lookup("Search/remote");
if(loggeduser == null && loggedadmin == null){
	Cookie[] c = request.getCookies();
	if(c!= null && c.length!=0) {
		for (Cookie coo: c) {			
			if (coo.getName().equals("loggeduser")) {
				RegisteredUser u = search.searchById(Long.parseLong(coo.getValue()));
				request.getSession().setAttribute("loggeduser", u);				
			} else if (coo.getName().equals("loggedadmin")) {
				Admin a = search.searchAdminById(Long.parseLong(coo.getValue()));
				request.getSession().setAttribute("loggedadmin", a);				
			}
		}
	}
}
//
%>
<body>
	<div id="top">
		<div id="topLeft">
			<a class="homeButton" href="/SwimV2Web"><img src="img/Home.png"/></a>
			<p class="title">SWIMv2</p>
			<p class="subtitle">Small World hypothesIs Machine v2</p>
		</div>
		<div id="topRight">
			<form id="searchBar" method="get" action="./SearchServlet">				
				<%if(loggeduser != null){%>
					<div class="searchRadioBox">
						<input type="radio" name="searchType" value="ability" checked>Search ability					
						<input type="radio" name="searchType" value="user">Search user
						<input type="checkbox" name="searchFilter" value="friends">Search only among friends
					</div>
					<img id="searchButton" src="img/Search.png" alt="search" title="search"/><input type="text" name="searchstring" value="insert ability or username...">
				<%}else{%>
					<input type="hidden" name="searchType" value="ability">
					<a href="#"><img id="searchButton" src="img/Search.png" alt="search" title="search"/></a><input type="text" name="searchstring" value="insert ability...">
				<%}%>								
			</form>			
				<%					
				if(loggeduser != null){				
				List<HelpRequest> helprequests = reqman.viewHelpRequests(loggeduser.getId());
				List<FriendshipRequest> friendshiprequests = reqman.viewFriendshipRequests(loggeduser.getId());
				List<HelpResponse> helpresponses = repman.viewHelpResponses(loggeduser.getId());
				List<FriendshipResponse> friendshipresponses = repman.viewFriendshipResponses(loggeduser.getId());
				List<AbilityResponse> abilityresponses = repman.viewAbilityResponses(loggeduser.getId());%>
				<div id="loggedUserMenu">
					<img class="loggedUserMenuPicture" src="<%= loggeduser.getPicture()%>"/>
					<p class="welcomeUser">Welcome <span style="color:#276733;"><%= loggeduser.getUsername()%></span>,</p>
					<p class="notifications"><a href="././personalpage.jsp" style="text-decoration:none;"><span class="notificationsNumber"><%= helprequests.size()+friendshiprequests.size()+helpresponses.size()+friendshipresponses.size()+abilityresponses.size()%></span></a> new notifications</p>
					<p class="logout"><a href="./LogoutServlet"><img src="img/Power.png"/><span>Log Out</span></a></p>
				</div>
				<%} else if (loggedadmin != null) {%>
				<div id="loggedUserMenu">
					<img class="loggedUserMenuPicture" src="users_pictures/admin.png"/>
					<p class="welcomeUser">Welcome <span style="color:#276733;"><%= loggedadmin.getUsername()%></span>,</p>
					<p class="logout"><a href="./LogoutServlet"><img src="img/Power.png"/><span>Log Out</span></a></p>
				</div>				
				<%} %>			
		</div>
	</div>
