	<div id="content" class="clearfix">
		<% 
		String pageMessage = (String)request.getSession().getAttribute("pageMessage");
		//System.out.println("PAGEMESSAGE: "+pageMessage);
		if (pageMessage!=null){
			String pageMessageType = (String)request.getSession().getAttribute("pageMessageType");	
			if(pageMessageType.equals("error")){
				%>				
				<div id="pageMessage" class="pageMessageError"><a href="#"><img class="pageMessageClose" src="img/close.png"/></a><%= pageMessage %></div>
				<%
			}else if(pageMessageType.equals("success")){
				%>
				<div id="pageMessage" class="pageMessageSuccess"><a href="#"><img class="pageMessageClose" src="img/close.png"/></a><%= pageMessage %></div>
				<%	
			}else if(pageMessageType.equals("warn")){
				%>
				<div id="pageMessage" class="pageMessageWarn"><a href="#"><img class="pageMessageClose" src="img/close.png"/></a><%= pageMessage %></div>
				<%	
			}
		}
		request.getSession().removeAttribute("pageMessage");
		request.getSession().removeAttribute("pageMessageType");
		%>