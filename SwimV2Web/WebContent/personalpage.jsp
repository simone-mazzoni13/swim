<%@ include file="frags/headers.jsp" %>
	<title>SWIMv2 | Personal Page</title>
</head>
<%@ include file="frags/top.jsp" %>
<%@ include file="frags/initialContent.jsp" %>
<%
if (loggeduser == null) {
	response.sendRedirect("./");
} else {
%>
	<% /*AFTER THIS LINE BEGINS THE CONTENT OF THE PAGE*/ %>
	<div id="personalPage">
	<div class="topRow clearfix">
		<div class="userPicture">
			<img src="<%= loggeduser.getPicture()%>" alt="<%= loggeduser.getUsername()%>_picture"/>
		</div>
		<div class="userData">
			<img class="userDataIco" src="img/userProfile.png" alt="Your Data">
			<div class="dataBox">
				<div class="userDataRow"><span class="fieldName">First Name: </span><span class="fieldData">${loggeduser.firstname}</span></div>
				<div class="userDataRow"><span class="fieldName">Last Name: </span><span class="fieldData">${loggeduser.lastname}</span></div>
				<div class="userDataRow"><span class="fieldName">E-mail: </span><span class="fieldData">${loggeduser.email}</span></div>
				<div class="userDataRow"><span class="fieldName">Username: </span><span class="fieldData">${loggeduser.username}</span></div>
			</div>
		</div>
		<%Set<Ability> userskills = userman.getSkills(loggeduser.getId());
		List<Ability> availableSkills = userman.getDeclarableSkills(loggeduser.getId());%>
		<div class="userSkills">
			<span class="abilitiesFieldName">Your abilities</span>
			<ul>						
			<%for(Ability a: userskills){%>
				<li class="userSkill<%= a.getId()%>">
					<div class="userSkill">
						<span class="abilityName"><%= a.getName()%></span>
						<form style="display:inline;" action="#">
							<input id="abilityName" type="hidden" value="<%= a.getName()%>">
							<input id="abilityId" type="hidden" name="abilityId" value="<%= a.getId()%>">
							<input id="userId" type="hidden" name="userId" value="<%= loggeduser.getId()%>">
							<img class="deleteAbilityButton" src="img/bt_delete.png"/>
						</form>
					</div>
				</li>
			<%}%>
			</ul>
			<form id="abilityComboContent" method="post" action="#">
				<input id="userId" type="hidden" value="<%= loggeduser.getId()%>">
				<select name="abilityCombo" class="abilityCombo">
				<% for(Ability av: availableSkills){%>				
					<option class="abilityId<%= av.getId()%>" value="<%= av.getId()%>"><%= av.getName()%></option>
				<%}%>					
				</select>
				<span class="addThisAbilityButton">Add this ability</span>
			</form>			
			<button id="addAbilityButton">Request new ability</button>			
		</div>
		
	</div>
	<h1 id="editUserDataButton">+ Edit your data</h1>
	<div id="editUserDataContent">
		<img class="whiteTriangle" src="img/whiteTriangle.png"/>
		<div id="editUserData">
			<form id="editUserDataForm" method="post" enctype="multipart/form-data" action="./EditInfoServlet">
				<div class="formrow"><div class="formMessage"></div></div>
				<div class="formrow"><label for="firstname">FIRST NAME</label><label for="lastname">LAST NAME</label></div>
				<div class="formrow"><input type="text" name="firstname" style="margin-right:10px;" value="<%= loggeduser.getFirstname()%>"><input type="text" name="lastname" value="<%= loggeduser.getLastname()%>"></div>
				<div class="formrow"><label for="oldpassword">CURRENT PASSWORD</label><label for="newpassword">NEW PASSWORD</label></div>
				<div class="formrow"><input type="password" name="oldpassword" style="margin-right:10px;"><input type="password" name="newpassword"></div>
				<div class="formrow"><label for="mail">MAIL</label><label for="picture">PICTURE</label></div>
				<div class="formrow"><input type="text" name="mail" style="margin-right:10px;" value="<%= loggeduser.getEmail()%>"><input type="file" name="picture"></div>
				<div class="formrow" style="margin-top:15px;text-align:left;margin-bottom:5px;"><input id="editFormButton" type="submit" value="Save"></div>
			</form>
		</div>
	</div>
	<%
	List<HelpRequest> helprequests = reqman.viewHelpRequests(loggeduser.getId());
	List<FriendshipRequest> friendshiprequests = reqman.viewFriendshipRequests(loggeduser.getId());
	List<HelpResponse> helpresponses = repman.viewHelpResponses(loggeduser.getId());
	List<FriendshipResponse> friendshipresponses = repman.viewFriendshipResponses(loggeduser.getId());
	List<AbilityResponse> abilityresponses = repman.viewAbilityResponses(loggeduser.getId());
	List<HelpResponse> feedbacks = repman.viewResponsesForFeedback(loggeduser.getId());
	int i=0;
	if(!helprequests.isEmpty()){%>
		<div id="helpRequestsList" class="personalPageList">
			<h1>+ Help Requests (<%= helprequests.size()%>)</h1>
			<%
			i=0;
			%><div class="listContent"><%
			for (HelpRequest r: helprequests) {
				if(i%2==0){%>
					<div class="requestWhite clearfix">
						<form method="post" action="viewrequest.jsp">
							<img src="<%= r.getSender().getPicture()%>"/>
							<p>from <%= r.getSender().getUsername()%></p>
							<p class="date">(<%= r.getDate().toString().substring(0,10)%>)</p>
							<input type="hidden" name="id_request" value="<%= r.getId()%>">
							<input type="hidden" name="requestType" value="help">
							<p style="text-align:right;"><input type="submit" value="View"></p>
						</form>
					</div>
				<%}else{%>
					<div class="requestGray clearfix">
						<form method="post" action="viewrequest.jsp">
							<img src="<%= r.getSender().getPicture()%>"/> 
							<p>from <%= r.getSender().getUsername()%></p>
							<p class="date">(<%= r.getDate().toString().substring(0,10)%>)</p>
							<input type="hidden" name="id_request" value="<%= r.getId()%>">
							<input type="hidden" name="requestType" value="help">
							<p style="text-align:right;"><input type="submit" value="View"></p>
						</form>
					</div>
				<%}
				i++;
			}%>
			</div>
		</div>
	<%}
	if(!friendshiprequests.isEmpty()){%>
	<div id="friendshipRequestsList" class="personalPageList">
		<h1>+ Friendship Requests (<%= friendshiprequests.size()%>)</h1>
		<%
		i=0;
		%><div class="listContent"><%
		for (FriendshipRequest r: friendshiprequests) {
			if(i%2==0){%>
				<div class="requestWhite clearfix">
					<form method="post" action="viewrequest.jsp">
						<img src="<%= r.getSender().getPicture()%>"/> 
						<p>from <%= r.getSender().getUsername()%></p>
						<p class="date">(<%= r.getDate().toString().substring(0,10)%>)</p>
						<input type="hidden" name="id_request" value="<%= r.getId()%>">
						<input type="hidden" name="requestType" value="friendship">
						<p style="text-align:right;"><input type="submit" value="View"></p>
					</form>
				</div>	
			<%}else{%>
				<div class="requestGray clearfix">
					<form method="post" action="viewrequest.jsp"> 
						<img src="<%= r.getSender().getPicture()%>"/>
						<p>from <%= r.getSender().getUsername()%></p>
						<p class="date">(<%= r.getDate().toString().substring(0,10)%>)</p>
						<input type="hidden" name="id_request" value="<%= r.getId()%>">
						<input type="hidden" name="requestType" value="friendship">
						<p style="text-align:right;"><input type="submit" value="View"></p>
					</form>
				</div>
			<%}
			i++;
		}%>
		</div>
	</div>
	<%}
	if(!helpresponses.isEmpty()){%>
	<div id="helpResponsesList" class="personalPageList">
		<h1>+ Help Responses  (<%= helpresponses.size()%>)</h1>
		<%
		i=0;
		%><div class="listContent"><%
		for (HelpResponse r: helpresponses) {
			if(i%2==0){%>
				<div class="requestWhite clearfix">
					<form method="post" action="viewresponse.jsp"> 
						<img src="<%= r.getRequest().getReceiver().getPicture()%>"/>
						<p>from <%= r.getRequest().getReceiver().getUsername()%></p>
						<p class="date">(<%= r.getRequest().getDate().toString().substring(0,10) %>)</p>
						<input type="hidden" name="id_response" value="<%= r.getId()%>">
						<input type="hidden" name="responseType" value="help">
						<p style="text-align:right;"><input type="submit" value="View"></p>
					</form>
				</div>	
			<%}else{%>
				<div class="requestGray clearfix">
					<form method="post" action="viewresponse.jsp"> 
						<img src="<%= r.getRequest().getReceiver().getPicture()%>"/>
						<p>from <%= r.getRequest().getReceiver().getUsername()%></p>
						<p class="date">(<%= r.getRequest().getDate().toString().substring(0,10) %>)</p>
						<input type="hidden" name="id_response" value="<%= r.getId()%>">
						<input type="hidden" name="responseType" value="help">
						<p style="text-align:right;"><input type="submit" value="View"></p>
					</form>
				</div>
			<%}
			i++;
		}%>
		</div>
	</div>
	<%}
	if(!friendshipresponses.isEmpty()){%>
	<div id="friendshipResponsesList" class="personalPageList">
		<h1>+ Friendship Responses  (<%= friendshipresponses.size()%>)</h1>
		<%
		i=0;
		%><div class="listContent"><%
		for (FriendshipResponse r: friendshipresponses) {
			if(i%2==0){%>
				<div class="requestWhite clearfix">
					<form method="post" action="viewresponse.jsp"> 
						<img src="<%= r.getRequest().getReceiver().getPicture()%>"/>
						<p>from <%= r.getRequest().getReceiver().getUsername()%></p>
						<p class="date">(<%= r.getRequest().getDate().toString().substring(0,10) %>)</p>
						<input type="hidden" name="id_response" value="<%= r.getId()%>">
						<input type="hidden" name="responseType" value="friendship">
						<p style="text-align:right;"><input type="submit" value="View"></p>
					</form>
				</div>	
			<%}else{%>
				<div class="requestGray clearfix">
					<form method="post" action="viewresponse.jsp"> 
						<img src="<%= r.getRequest().getReceiver().getPicture()%>"/>
						<p>from <%= r.getRequest().getReceiver().getUsername()%></p>
						<p class="date">(<%= r.getRequest().getDate().toString().substring(0,10) %>)</p>
						<input type="hidden" name="id_response" value="<%= r.getId()%>">
						<input type="hidden" name="responseType" value="friendship">
						<p style="text-align:right;"><input type="submit" value="View"></p>
					</form>
				</div>
			<%}
			i++;
		}%>
		</div>
	</div>
	<%}
	if(!abilityresponses.isEmpty()){%>
		<div id="abilityResponsesList" class="personalPageList">
			<h1>+ Ability Responses (<%= abilityresponses.size()%>)</h1>
			<%
			i=0;
			%><div class="listContent"><%
			for (AbilityResponse r: abilityresponses) {
				if(i%2==0){%>
					<div class="requestWhite clearfix">
						<form method="post" action="viewresponse.jsp"> 
							<img src="users_pictures/admin.png"/>
							<p>from <%= r.getAdmin().getUsername()%></p>
							<p>(<%= r.getRequest().getDate().toString().substring(0,10) %>)</p>
							<input type="hidden" name="id_response" value="<%= r.getId()%>">
							<input type="hidden" name="responseType" value="ability">
							<p style="text-align:right;"><input type="submit" value="View"></p>
						</form>
					</div>
				<%}else{%>
					<div class="requestGray clearfix">
						<form method="post" action="viewresponse.jsp"> 
							<img src="users_pictures/admin.png"/>
							<p>from <%= r.getAdmin().getUsername()%></p>
							<p>(<%= r.getRequest().getDate().toString().substring(0,10) %>)</p>
							<input type="hidden" name="id_response" value="<%= r.getId()%>">
							<input type="hidden" name="responseType" value="ability">
							<p style="text-align:right;"><input type="submit" value="View"></p>
						</form>
					</div>
				<%}
				i++;
			}%>
			</div>
		</div>
	<%}
	if(!feedbacks.isEmpty()){%>
	<div id="feedbacksList" class="personalPageList">
		<h1>+ Leave feedback (<%= feedbacks.size()%>)</h1>
		<%
		i=0;
		%><div class="listContent"><%
		for (HelpResponse r: feedbacks) {
			if(i%2==0){%>
				<div class="requestWhite clearfix">
					<form method="post" action="viewresponse.jsp"> 
						<img src="<%= r.getRequest().getReceiver().getPicture()%>"/>
						<p>from <%= r.getRequest().getReceiver().getUsername()%></p>
						<p class="date">(<%= r.getRequest().getDate().toString().substring(0,10) %>)</p>
						<input type="hidden" name="id_response" value="<%= r.getId()%>">
						<input type="hidden" name="responseType" value="feedback">
						<p style="text-align:right;"><input type="submit" value="Send Feedback"></p>
					</form>
				</div>	
			<%}else{%>
				<div class="requestGray clearfix">
					<form method="post" action="viewresponse.jsp"> 
						<img src="<%= r.getRequest().getReceiver().getPicture()%>"/>
						<p>from <%= r.getRequest().getReceiver().getUsername()%></p>
						<p class="date">(<%= r.getRequest().getDate().toString().substring(0,10) %>)</p>
						<input type="hidden" name="id_response" value="<%= r.getId()%>">
						<input type="hidden" name="responseType" value="feedback">
						<p style="text-align:right;"><input type="submit" value="Send Feedback"></p>
					</form>
				</div>
			<%}
			i++;
		}%>
		</div>
	</div>
	<%}%>
<%}%>
</div>
<%@ include file="frags/footer.jsp" %>