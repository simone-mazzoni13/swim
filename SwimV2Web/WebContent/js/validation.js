$(document).ready(function() {
	function validate(form){
		var emailFilter = /^[^@]+@[^@.]+\.[^@]*\w\w$/ ;
	    var illegalChars= /[\(\)\<\>\,\;\:\\\"\[\]]/ ;
	    var messageError = 'ERRORS:\n';
	    var number_errors = 0;
		console.log(form);
		firstname = $("#register input[name='firstname']");
		lastname= $("#register input[name='lastname']");
		username= $("#register input[name='username']");
		password= $("#register input[name='password']");
		mail = $("#register input[name='mail']");
		//console.log(username);
		console.log($("#register input"));
		$("#register input").each(function(){
			$(this).css('border','2px inset');
		});
		
		if(firstname.val()==''){
			number_errors++;
			messageError += "- FIRST NAME can't be empty\n";
			firstname.css('border','2px solid #ff0000');
		}
		if(lastname.val()==''){
			number_errors++;
			messageError += "- LAST NAME can't be empty\n";
			lastname.css('border','2px solid #ff0000');
		}
		if(username.val()==''){
			number_errors++;
			messageError += "- USERNAME can't be empty\n";
			username.css('border','2px solid #ff0000');
		}else if(illegalChars.test(username.val())){
			number_errors++;
			messageError += "- illegal character for USERNAME\n";
			username.css('border','2px solid #ff0000');
		}
		if(password.val()==''){
			number_errors++;
			messageError += "- PASSWORD can't be empty\n";
			password.css('border','2px solid #ff0000');
		}else if(illegalChars.test(password.val())){
			number_errors++;
			messageError += "- illegal character for PASSWORD\n";
			password.css('border','2px solid #ff0000');
		}
		if(mail.val()==''){
			number_errors++;
			messageError += "- MAIL can't be empty\n";
			mail.css('border','2px solid #ff0000');
		}else if(!emailFilter.test(mail.val())){
			number_errors++;
			messageError += "- invalid MAIL address\n";
			mail.css('border','2px solid #ff0000');
		}
		
		if(number_errors > 0){
			messageError = number_errors+' '+messageError;		
			alert(messageError);
		}else{
			form.submit();
		}
	}
	var myform = $('#register'); 
	$('#registerFormButton').click(function(){
		console.log("myform: "+myform);
		validate(myform);
	});
	
	//Controllo se firstname  vuoto o meno
	$('#register input[name="firstname"]').blur(function(){
		if(this.value==''){
			$('#register input[name="firstname"]').css('border','2px solid #ff0000');
		}else{
			$('#register input[name="firstname"]').css('border','2px solid #00ff00');
		}
	});
	
	//Controllo se lastname  vuoto o meno
	$('#register input[name="lastname"]').blur(function(){
		if(this.value==''){
			$('#register input[name="lastname"]').css('border','2px solid #ff0000');
		}else{
			$('#register input[name="lastname"]').css('border','2px solid #00ff00');
		}
	});
	
	//Controllo se password  vuoto o meno
	$('#register input[name="password"]').blur(function(){
		if(this.value==''){
			$('#register input[name="password"]').css('border','2px solid #ff0000');
		}else{
			$('#register input[name="password"]').css('border','2px solid #00ff00');
		}
	});
	 
	//Controllo se username  vuoto o gi presente nel DB o meno
	$('#register input[name="username"]').blur(function(){
		var illegalChars= /[\(\)\<\>\,\;\:\\\"\[\]]/ ;
		if(this.value==''){
			$('#register input[name="username"]').css('border','2px solid #ff0000');
			$('#register .formMessage').html('').fadeOut(1000);
			$('.formcontainer').animate({height: '325px'},1000);
		}else{
			if(illegalChars.test(this.value)){
				message = 'ERROR: invalid characters in username';
				$('#register input[name="mail"]').css('border','2px solid #ff0000');					
				$('#register .formMessage').html(message).fadeIn(1000);
				$('.formcontainer').animate({height: '375px'},1000);
			}else{
				var message = 'ERROR: username already registered';
				$.ajax({
					url: './UsernameCheckServlet',
					type: 'post',
					dataType: 'text',
					data: 'username='+this.value,
					success: function(data) {
						var result = eval(data); //result sar true se data == "true", false se data =="false"
						if(result==true){					
							$('#register input[name="username"]').css('border','2px solid #00ff00');
							$('#register .formMessage').html('').fadeOut(1000);
							$('.formcontainer').animate({height: '325px'},1000);
						}else if(result==false){
							$('#register input[name="username"]').css('border','2px solid #ff0000');							
							$('#register .formMessage').html(message).fadeIn(1000);
							$('.formcontainer').animate({height: '375px'},1000);
						}
					}
				});
			}
		}
	});
	
	//Controllo se mail  vuoto o gi presente nel DB o meno
	$('#register input[name="mail"]').blur(function(){
		var emailFilter = /^[^@]+@[^@.]+\.[^@]*\w\w$/ ;
		if(this.value==''){
			$('#register input[name="mail"]').css('border','2px solid #ff0000');
			$('#register .formMessage').html('').fadeOut(1000);
			$('.formcontainer').animate({height: '325px'},1000);
		}else{
			if(!emailFilter.test(this.value)){
				message = 'ERROR: invalid mail address';
				$('#register input[name="mail"]').css('border','2px solid #ff0000');					
				$('#register .formMessage').html(message).fadeIn(1000);
				$('.formcontainer').animate({height: '375px'},1000);
			}else{
				message = 'ERROR: mail address already registered';
				$.ajax({
					url: './MailCheckServlet',
					type: 'post',
					dataType: 'text',
					data: 'mail='+this.value,
					success: function(data) {
						var result = eval(data); //result sar true se data == "true", false se data =="false"
						if(result==true){					
							$('#register input[name="mail"]').css('border','2px solid #00ff00');
							$('#register .formMessage').html('').fadeOut(1000);
							$('.formcontainer').animate({height: '325px'},1000);
						}else if(result==false){
							$('#register input[name="mail"]').css('border','2px solid #ff0000');					
							$('#register .formMessage').html(message).fadeIn(1000);
							$('.formcontainer').animate({height: '375px'},1000);
						}
					}
				});
			}
		}
	});
	
	
	/*************************************************** END REGISTRATION VALIDATION *********************************************************/
	
	function validateEdit(form){
		var emailFilter = /^[^@]+@[^@.]+\.[^@]*\w\w$/ ;
	    var illegalChars= /[\(\)\<\>\,\;\:\\\"\[\]]/ ;
	    var messageError = 'ERRORS:\n';
	    var number_errors = 0;
		console.log(form);
		firstname = $("input[name='firstname']");
		lastname= $("input[name='lastname']");
		oldpassword= $("input[name='oldpassword']");
		password= $("input[name='newpassword']");
		mail = $("input[name='mail']");
		$("#editUserDataForm input").each(function(){
			$(this).css('border','2px inset');
		});
		
		if(firstname.val()==''){
			number_errors++;
			messageError += "- FIRST NAME can't be empty\n";
			firstname.css('border','2px solid #ff0000');
		}
		if(lastname.val()==''){
			number_errors++;
			messageError += "- LAST NAME can't be empty\n";
			lastname.css('border','2px solid #ff0000');
		}
		if(username.val()==''){
			number_errors++;
			messageError += "- USERNAME can't be empty\n";
			username.css('border','2px solid #ff0000');
		}else if(illegalChars.test(username.val())){
			number_errors++;
			messageError += "- illegal character for USERNAME\n";
			username.css('border','2px solid #ff0000');
		}
		if(password.val()==''){
			number_errors++;
			messageError += "- PASSWORD can't be empty\n";
			password.css('border','2px solid #ff0000');
		}else if(illegalChars.test(password.val())){
			number_errors++;
			messageError += "- illegal character for PASSWORD\n";
			password.css('border','2px solid #ff0000');
		}
		if(mail.val()==''){
			number_errors++;
			messageError += "- MAIL can't be empty\n";
			mail.css('border','2px solid #ff0000');
		}else if(!emailFilter.test(mail.val())){
			number_errors++;
			messageError += "- invalid MAIL address\n";
			mail.css('border','2px solid #ff0000');
		}
		
		if(number_errors > 0){
			messageError = number_errors+' '+messageError;		
			alert(messageError);
		}else{
			form.submit();
		}
	}
	var myEditform = $('#editUserDataForm'); 
	$('#editUserDataForm #editFormButton').click(function(){
		validateEdit(myEditform);
	});
	
	//Controllo se firstname  vuoto o meno
	$('#editUserDataForm input[name="firstname"]').blur(function(){
		if(this.value==''){
			$('#editUserDataForm input[name="firstname"]').css('border','2px solid #ff0000');
		}else{
			$('#editUserDataForm input[name="firstname"]').css('border','2px solid #00ff00');
		}
	});
	
	//Controllo se lastname  vuoto o meno
	$('#editUserDataForm input[name="lastname"]').blur(function(){
		if(this.value==''){
			$('#editUserDataForm input[name="lastname"]').css('border','2px solid #ff0000');
		}else{
			$('#editUserDataForm input[name="lastname"]').css('border','2px solid #00ff00');
		}
	});
	
	//Controllo se oldpassword  vuoto o meno
	$('#editUserDataForm input[name="oldpassword"]').blur(function(){
		if(this.value==''){
			$('#editUserDataForm input[name="oldpassword"]').css('border','2px solid #ff0000');
		}else{
			$('#editUserDataForm input[name="oldpassword"]').css('border','2px solid #00ff00');
		}
	});
	
	//Controllo se newpassword  vuoto o meno
	$('#editUserDataForm input[name="newpassword"]').blur(function(){
		if(this.value==''){
			$('#editUserDataForm input[name="newpassword"]').css('border','2px solid #ff0000');
		}else{
			$('#editUserDataForm input[name="newpassword"]').css('border','2px solid #00ff00');
		}
	});
	
	//Controllo se mail  vuoto o gi presente nel DB o meno
	$('#editUserDataForm input[name="mail"]').blur(function(){
		var emailFilter = /^[^@]+@[^@.]+\.[^@]*\w\w$/ ;
		if(this.value==''){
			$('#editUserDataForm input[name="mail"]').css('border','2px solid #ff0000');
			$('#editUserDataForm .formMessage').html('').fadeOut(1000);
			$('.formcontainer').animate({height: '325px'},1000);
		}else{
			if(!emailFilter.test(this.value)){
				message = 'ERROR: invalid mail address';
				$('#editUserDataForm input[name="mail"]').css('border','2px solid #ff0000');					
				$('#editUserDataForm .formMessage').html(message).fadeIn(1000);
				$('.formcontainer').animate({height: '375px'},1000);
			}else{
				message = 'ERROR: mail address already registered';
				$.ajax({
					url: './MailCheckServlet',
					type: 'post',
					dataType: 'text',
					data: 'mail='+this.value,
					success: function(data) {
						var result = eval(data); //result sar true se data == "true", false se data =="false"
						if(result==true){					
							$('#editUserDataForm input[name="mail"]').css('border','2px solid #00ff00');
							$('#editUserDataForm .formMessage').html('').fadeOut(1000);
							$('.formcontainer').animate({height: '325px'},1000);
						}else if(result==false){
							$('#editUserDataForm input[name="mail"]').css('border','2px solid #ff0000');					
							$('#editUserDataForm .formMessage').html(message).fadeIn(1000);
							$('.formcontainer').animate({height: '375px'},1000);
						}
					}
				});
			}
		}
	});
	
});