<%@ include file="frags/headers.jsp" %>
	<title>SWIMv2 | Search</title>
</head>
<%@ include file="frags/top.jsp" %>
<%@ include file="frags/initialContent.jsp" %>
<% /*AFTER THIS LINE BEGINS THE CONTENT OF THE PAGE*/ %>
<% 
Set<RegisteredUser> results = (Set<RegisteredUser>)request.getAttribute("searchresults");
String searchstring = (String)request.getParameter("searchstring");
if (results.isEmpty()){%>
	<div class="resultNumber clearfix"><img src="img/Users.png"/><p>Results found for <span style="color:#276733;">"<%= searchstring%>"</span>: (<span style="color:#276733;"><%= results.size()%></span>)</p></div>
	<p class="noResults">Sorry, no results found!! Try another value!!</p>	
<%} else {
	for(RegisteredUser utente: results) {
		if (utente.equals(loggeduser)) {
			results.remove(utente);
			break;
		}
	}
	%>
	<div class="resultNumber clearfix"><img src="img/Users.png"/><p>Results found for <span style="color:#276733;">"<%= searchstring%>"</span>: (<span style="color:#276733;"><%= results.size()%></span>)</p></div>
	<div class="results">
	<%
	int i=0;
	for(RegisteredUser u: results) {
		Set<Ability> skills = userman.getSkills(u.getId());
			if(i%2 == 0){%>
				<div class="resultObj clearfix white">
			<%}else{%>
				<div class="resultObj clearfix gray">
			<%}%>
				<div class="searchUserPicture"><img src="<%= u.getPicture()%>"/></div>
				<div class="searchUserDataContent">
					<div class="searchUserData">
						<div class="searchUserDataRow"><span class="fieldName">Username: </span><span class="fieldData"><%= u.getUsername()%></span></div>
						<div class="searchUserDataRow"><span class="fieldName">First Name: </span><span class="fieldData"><%= u.getFirstname()%></span></div>
						<div class="searchUserDataRow"><span class="fieldName">Last Name: </span><span class="fieldData"><%= u.getLastname()%></span></div>				
						<div class="searchUserDataRow"><span class="fieldName">E-mail: </span><span class="fieldData"><%= u.getEmail()%></span></div>
					</div>
					<%if(!skills.isEmpty()){%>
					<div class="searchUserSkills">
						<span class="fieldName">Abilities:</span>
						<ul>						
						<%												
							for(Ability a:skills) {
								out.print("<li>");
								out.print(a.getName());
								out.print("</li>");
							}
						%>
						</ul>
					</div>
					<%}%>				
				</div>				
					<%if(loggeduser != null){%>
					<div class="searchUserOptionsLogged">
						<% if(friendman.isFriend(loggeduser.getId(), u.getId())) {%>
						<form class="sendFriendshipRequest" method="post" action="#">
							<input class="sendFriendshipRequestButton" value="Friends" type="button">
						</form>
						<%} else if (friendman.isFriendshipRequested(loggeduser.getId(), u.getId())) { %>						
						<button class="waitingFriendshipRequest">Richiesta di Amicizia in Sospeso</button>								
						<%} else { %>
						<form class="sendFriendshipRequest" method="post" action="./SendRequestServlet">
							<input class="sendFriendshipRequestButton" value="+1 Add Friend" type="button">
							<input type="hidden" name="sender" value="<%= loggeduser.getId()%>">
							<input type="hidden" name="receiver" value="<%= u.getId()%>">
							<input type="hidden" name="receiverName" value="<%= u.getUsername()%>">
							<input type="hidden" name="requestType" value="friendship">
						</form>
						<%} %>										
						<form class="sendHelpRequest" method="post" action="./SendRequestServlet">
							<input class="sendHelpRequestButton" value="Ask For Help" type="button">
							<input type="hidden" name="sender" value="<%= loggeduser.getId()%>">
							<input type="hidden" name="receiver" value="<%= u.getId()%>">
							<input type="hidden" name="receiverName" value="<%= u.getUsername()%>">
							<input type="hidden" name="requestType" value="help">
						</form>
					<%}else{%>
						<div class="searchUserOptionsUnlogged">
							<a href="/SwimV2Web">Register or Login to be able to ask for help</a>
					<%}%>	
				</div>
			</div>
		<%i++;%>	
	<%}%>
		</div>
<%}%>
<%@ include file="frags/footer.jsp" %>
