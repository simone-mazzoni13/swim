package pack1;

import java.io.IOException;

import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import swim.entitybeans.FriendshipRequest;
import swim.entitybeans.HelpRequest;
import swim.sessionbeans.FriendshipManagerRemote;
import swim.sessionbeans.RequestRemote;
import swim.sessionbeans.ResponseRemote;

/**
 * Servlet implementation class SendResponseServlet
 */
public class SendResponseServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SendResponseServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			InitialContext jndiContext = new InitialContext();
			ResponseRemote repman = (ResponseRemote) jndiContext.lookup("Response/remote");
			RequestRemote requestman = (RequestRemote) jndiContext.lookup("Request/remote");
			FriendshipManagerRemote friendman = (FriendshipManagerRemote) jndiContext.lookup("FriendshipManager/remote");
			String responseType = request.getParameter("responseType");
			long id_request = Long.parseLong(request.getParameter("id_request"));
			String message = request.getParameter("message");
			boolean result;
			if (request.getParameter("result").equals("Accept")) {
				result = true;
			} else {
				result = false;
			}
			if (responseType.equals("help")) {
				repman.sendHelpResponse(id_request, result, message);
				request.getSession().setAttribute("pageMessage", "Your reply was succesfully sent!");
				request.getSession().setAttribute("pageMessageType", "success");
				response.sendRedirect("personalpage.jsp");
			} else if (responseType.equals("friendship")) {
				repman.sendFriendshipResponse(id_request, result, message);
				FriendshipRequest req = requestman.viewFriendshipRequest(id_request);
				if (result == true) {
					friendman.addFriendship(req.getSender().getId(), req.getReceiver().getId());
					if (req.getSuggested() == false) {
						request.getSession().setAttribute("pageMessage", "You accepted " + req.getSender().getUsername() + "'s friendship request. Your reply was sent succesfully.");
						request.getSession().setAttribute("pageMessageType", "success");
						String redurl = response.encodeRedirectURL("suggestionpage.jsp?otheruser=" + req.getSender().getUsername());
						response.sendRedirect(redurl);
					} else {
						request.getSession().setAttribute("pageMessage", "You accepted " + req.getSender().getUsername() + "'s friendship request. Your reply was sent succesfully.");
						request.getSession().setAttribute("pageMessageType", "success");
						response.sendRedirect("personalpage.jsp");
					}
				} else {
					request.getSession().setAttribute("pageMessage", "You declined " + req.getSender().getUsername() + "'s friendship request. Your reply was sent succesfully.");
					request.getSession().setAttribute("pageMessageType", "success");
					response.sendRedirect("personalpage.jsp");
				}
				
			} else if (responseType.equals("ability")) {
				repman.sendAbilityResponse(id_request, result, message, Long.parseLong(request.getParameter("id_admin")));
				request.getSession().setAttribute("pageMessage", "Your reply was succesfully sent!");
				request.getSession().setAttribute("pageMessageType", "success");
				response.sendRedirect("personalpage.jsp");
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

}
