package pack1;

import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Set;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.jasper.tagplugins.jstl.core.Out;

import swim.entitybeans.RegisteredUser;
import swim.sessionbeans.SearchRemote;
import swim.sessionbeans.UserManagerRemote;
import swim.sessionbeans.Search;

/**
 * Servlet implementation class UsernameCheckServlet
 */
public class UsernameCheckServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UsernameCheckServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			System.out.println("UsernameCheck");
			InitialContext jndiContext = new InitialContext();
			SearchRemote src = (SearchRemote) jndiContext.lookup("Search/remote");			
			String username = request.getParameter("username");
			Set<RegisteredUser> users = src.searchByUsername(username);
			String usernameOK = null;			
			if(users.isEmpty()){
				usernameOK = "true";
			}else{
				usernameOK = "false";
			}
			response.setContentType("text/plain");  
			response.setCharacterEncoding("UTF-8"); 
			response.getWriter().write(usernameOK);
			//request.getRequestDispatcher("index.html").forward(request, response);
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
