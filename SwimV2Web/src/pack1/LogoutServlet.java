package pack1;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class LogoutServlet
 */
public class LogoutServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LogoutServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			request.getSession().removeAttribute("loggeduser");
			request.getSession().removeAttribute("loggedadmin");
			Cookie c[] = request.getCookies();
			for (Cookie coo:c) {
				if (coo.getName().equals("loggeduser") || coo.getName().equals("loggedadmin") ) {
					coo.setMaxAge(0);
					response.addCookie(coo);
				}
			}
			response.sendRedirect("./");
		} catch (Exception e) {
			e.printStackTrace();
			response.sendRedirect(request.getHeader("Referer"));
		}
	}

}
