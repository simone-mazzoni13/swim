package pack1;

import java.io.IOException;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import swim.sessionbeans.UserManagerRemote;

/**
 * Servlet implementation class DeleteAbilityServlet
 */
public class DeleteAbilityServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DeleteAbilityServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("DeleteAbility");
		try {
			InitialContext jndiContext = new InitialContext();
			UserManagerRemote userman = (UserManagerRemote) jndiContext.lookup("UserManager/remote");
			long userId = Long.parseLong(request.getParameter("userId"));
			long abilityId = Long.parseLong(request.getParameter("abilityId"));
			System.out.println("USERID: "+userId+" - ABILITYID: "+abilityId);
			boolean result = userman.deleteSkill(userId, abilityId);
			System.out.println("DELETED");
			response.setContentType("text/plain");  
			response.setCharacterEncoding("UTF-8"); 
			response.getWriter().write(Boolean.toString(result));
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
