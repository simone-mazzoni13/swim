package pack1;

import java.io.IOException;

import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import swim.sessionbeans.RequestRemote;


/**
 * Servlet implementation class SendRequestServlet
 */
public class SendRequestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SendRequestServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {			
			InitialContext jndiContext = new InitialContext();
			RequestRemote req = (RequestRemote) jndiContext.lookup("Request/remote");
			String requestType = request.getParameter("requestType");
			String backUrl = request.getHeader("Referer");
			System.out.println("BACKURL: "+backUrl);
			if (requestType.equals("friendship")) {
				boolean suggested = false;
				if (request.getParameter("suggested") != null) {
					suggested = true;
				}
				req.sendFriendshipRequest(Long.parseLong(request.getParameter("sender")), Long.parseLong(request.getParameter("receiver")), request.getParameter("message"), suggested);				
				request.getSession().setAttribute("pageMessage", "Your friendship request for the user "+request.getParameter("receiverName")+" was succesfully sent!");
				request.getSession().setAttribute("pageMessageType", "success");
				response.sendRedirect(backUrl);
			} else if(requestType.equals("help")) {
				req.sendHelpRequest(Long.parseLong(request.getParameter("sender")), Long.parseLong(request.getParameter("receiver")), request.getParameter("message"));
				request.getSession().setAttribute("pageMessage", "Your help request for the user "+request.getParameter("receiverName")+" was succesfully sent!");
				request.getSession().setAttribute("pageMessageType", "success");
				response.sendRedirect(backUrl);
			} else if (requestType.equals("ability")) {
				req.sendAbilityRequest(Long.parseLong(request.getParameter("sender")), request.getParameter("message"));
				request.getSession().setAttribute("pageMessage", "Your request was succesfully sent to an administrator!");
				request.getSession().setAttribute("pageMessageType", "success");
				response.sendRedirect("./personalpage.jsp");
			} else {
				
			}
		} catch(Exception e) {
			e.printStackTrace();
			
		} 
			
		
		
	}

}
