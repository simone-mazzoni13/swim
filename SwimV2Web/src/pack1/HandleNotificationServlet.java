package pack1;

import java.io.IOException;

import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import swim.entitybeans.FriendshipRequest;
import swim.sessionbeans.RequestRemote;

/**
 * Servlet implementation class HandleNotificationServlet
 */
public class HandleNotificationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HandleNotificationServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			if (request.getParameter("result").equals("Cancel")) {
				response.sendRedirect("./personalpage.jsp");
			} else if (request.getParameter("result").equals("Ok")) {
				InitialContext jndiContext = new InitialContext();
				RequestRemote reqman = (RequestRemote) jndiContext.lookup("Request/remote");
				String requestType = request.getParameter("responseType");
				long id_request = Long.parseLong(request.getParameter("id_request"));
				reqman.setNotificated(id_request, requestType);
				if (requestType.equals("friendship")) {
					FriendshipRequest req = reqman.viewFriendshipRequest(id_request);
					if (request.getParameter("response") != null && req.getSuggested() == false) {
						String redurl = response.encodeRedirectURL("suggestionpage.jsp?otheruser=" + req.getReceiver().getUsername());
						response.sendRedirect(redurl);
					} else {
						response.sendRedirect("./personalpage.jsp");
					}
				} else {
					response.sendRedirect("./personalpage.jsp");
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
			
		}
		
	}

}
