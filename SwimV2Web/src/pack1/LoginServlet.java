package pack1;

import java.io.IOException;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.jasper.tagplugins.jstl.core.Out;

import swim.entitybeans.RegisteredUser;
import swim.sessionbeans.AuthenticationRemote;
import swim.entitybeans.Admin;

/**
 * Servlet implementation class LoginServlet
 */
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			InitialContext jndiContext = new InitialContext();
			AuthenticationRemote man = (AuthenticationRemote) jndiContext.lookup("Authentication/remote");
			String username = request.getParameter("username");
			String password = request.getParameter("password");
			request.getSession().removeAttribute("failedlogin");
			if (request.getParameter("admin") != null) {
				System.out.println("Login admin");
				Admin admin = (Admin) man.AuthenticateAdmin(username, password);
				if (admin != null) {
					request.getSession().removeAttribute("pageMessage");
					request.getSession().removeAttribute("pageMessageType");
					request.getSession().setAttribute("loggedadmin", admin);
					if (request.getParameter("keepmelogged")!= null) {
						Cookie admincookie = new Cookie("loggedadmin", Long.toString(admin.getId()));
						admincookie.setMaxAge(60*60*24*7);
						admincookie.setPath(request.getServerName());
						response.addCookie(admincookie);
					}
					response.sendRedirect("administrationpage.jsp");
				} else {
					request.getSession().setAttribute("pageMessage", "ERROR: wrong username and/or password");
					request.getSession().setAttribute("pageMessageType", "error");
					response.sendRedirect("./");
				}
			} else {
				System.out.println("Login utente");
				RegisteredUser user = (RegisteredUser) man.AuthenticateUser(username, password);
				if (user != null) {
					request.getSession().removeAttribute("pageMessage");
					request.getSession().removeAttribute("pageMessageType");
					request.getSession().setAttribute("loggeduser", user);
					if (request.getParameter("keepmelogged")!= null) {
						Cookie usercookie = new Cookie("loggeduser", Long.toString(user.getId()));
						usercookie.setMaxAge(60*60*24*7);
						System.out.print(request.getServerName());
						usercookie.setPath(request.getServerName());
						response.addCookie(usercookie);
					}
					response.sendRedirect("personalpage.jsp");
				} else {
					request.getSession().setAttribute("pageMessage", "ERROR: wrong username and/or password");
					request.getSession().setAttribute("pageMessageType", "error");
					response.sendRedirect("./");
				}
			}
			
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			request.getSession().setAttribute("pageMessage", "ERROR: login failed");
			request.getSession().setAttribute("pageMessageType", "error");
			response.sendRedirect("./");
		}	
	}

}
