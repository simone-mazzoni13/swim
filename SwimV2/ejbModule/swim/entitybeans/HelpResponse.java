package swim.entitybeans;


import javax.persistence.*;


@Entity
@Table(name = "helpresponse")
public class HelpResponse implements java.io.Serializable {
	
	@Id
	@GeneratedValue
	@Column(name = "ID")	
	private long id;
	
	@OneToOne(targetEntity=swim.entitybeans.HelpRequest.class)
	@JoinColumn(name = "id_help_request")
	private HelpRequest request;
	
	@Column(name = "response")	
	private boolean response;
	
	@Column(name = "message", columnDefinition = "LONGTEXT")	
	private String message;
	
	public long getId() {
		return id;
	}
	
	public HelpRequest getRequest() {
		return request;
	}

	public void setRequest(HelpRequest request) {
		this.request = request;
	}
	
	public boolean getResponse() {
		return response;
	}

	public void setResponse(boolean response) {
		this.response = response;
	}
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	

}
