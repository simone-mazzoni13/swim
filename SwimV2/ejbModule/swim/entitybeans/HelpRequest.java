package swim.entitybeans;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "helprequest")
public class HelpRequest implements java.io.Serializable {

	@Id
	@GeneratedValue
	@Column(name = "ID")	
	private long id;
	
	@ManyToOne(targetEntity=swim.entitybeans.RegisteredUser.class)
	@JoinColumn(name = "id_sender")
	private RegisteredUser sender;
	
	@ManyToOne(targetEntity=swim.entitybeans.RegisteredUser.class)
	@JoinColumn(name = "id_receiver")
	private RegisteredUser receiver;
	
	@Column(name = "state")	
	private boolean state;
	
	@Column(name = "notification")	
	private boolean notification;
	
	@Column(name = "message", columnDefinition = "LONGTEXT")	
	private String message;
	
	@Column(name = "date")	
	private Date date;
	
	public long getId() {
		return id;
	}
	
	public RegisteredUser getSender() {
		return sender;
	}

	public void setSender(RegisteredUser sender) {
		this.sender = sender;
	}
	
	public RegisteredUser getReceiver() {
		return receiver;
	}

	public void setReceiver(RegisteredUser receiver) {
		this.receiver = receiver;
	}
	
	public boolean getState() {
		return state;
	}

	public void setState(boolean state) {
		this.state = state;
	}
	
	public boolean getNotification() {
		return notification;
	}

	public void setNotification(boolean notification) {
		this.notification = notification;
	}
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
}
