package swim.entitybeans;

import java.util.Set;

import javax.persistence.*;

@Entity
@Table(name = "registereduser")
public class RegisteredUser {
	@Id
	@GeneratedValue
	@Column(name = "ID")	
	private long id;

	@Column(name = "username")	
	private String username;
	
	@Column(name = "password")	
	private String password;
	
	@Column(name = "email")	
	private String email;
	
	@Column(name = "firstname")	
	private String firstname;
	
	@Column(name = "lastname")	
	private String lastname;
	
	@Column(name = "picture")	
	private String picture;
	
	@ManyToMany(targetEntity = Ability.class)
	@JoinTable(name="skills", joinColumns= @JoinColumn(name="id_user", referencedColumnName="ID"), inverseJoinColumns=@JoinColumn(name="id_ability", referencedColumnName="ID"))
	private Set<Ability> skills;
	
	@ManyToMany(targetEntity = RegisteredUser.class)
	@JoinTable(name="friendship", joinColumns= @JoinColumn(name="id_user1", referencedColumnName="ID"), inverseJoinColumns=@JoinColumn(name="id_user2", referencedColumnName="ID"))
	private Set<RegisteredUser> friends;
	
	@Override
	public boolean equals(Object other) {
		try {
			RegisteredUser u = (RegisteredUser) other;
			if (u == null) {
				return false;
			} else if (this.getId() == u.getId()) {
				return true;
			} else {
				return false;
			}	
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}			
	}
	
	public long getId() {
		return id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public Set<Ability> getSkills() {
		return skills;
	}

	public void setSkills(Set<Ability> skills) {
		this.skills = skills;
	}

	public Set<RegisteredUser> getFriends() {
		return friends;
	}
	
	public void setFriends(Set<RegisteredUser> friends) {
		this.friends = friends;
	}

}
