package swim.entitybeans;


import javax.persistence.*;

@Entity
@Table(name = "ability")
public class Ability {

	@Id
	@GeneratedValue
	@Column(name = "ID")	
	private long id;

	@Column(name = "Name")	
	private String name;
	
	public long getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
