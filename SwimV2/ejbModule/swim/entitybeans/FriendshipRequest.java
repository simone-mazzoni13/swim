package swim.entitybeans;

import javax.persistence.*;


import java.util.Date;


@Entity
@Table(name = "friendshiprequest")
public class FriendshipRequest implements java.io.Serializable {
	
	@Id
	@GeneratedValue
	@Column(name = "ID")	
	private long id;
	
	@ManyToOne(targetEntity=swim.entitybeans.RegisteredUser.class)
	@JoinColumn(name = "id_sender")
	private RegisteredUser sender;
	
	@ManyToOne(targetEntity=swim.entitybeans.RegisteredUser.class)
	@JoinColumn(name = "id_receiver")
	private RegisteredUser receiver;
	
	@Column(name = "state")	
	private boolean state;
	
	@Column(name = "notificated")	
	private boolean notificated;
	
	@Column(name = "suggested")	
	private boolean suggested;
	
	@Column(name = "message", columnDefinition = "LONGTEXT")	
	private String message;
	
	@Column(name = "date")	
	private Date date;
	
	public long getId() {
		return id;
	}
	
	public RegisteredUser getSender() {
		return sender;
	}

	public void setSender(RegisteredUser sender) {
		this.sender = sender;
	}
	
	public RegisteredUser getReceiver() {
		return receiver;
	}

	public void setReceiver(RegisteredUser receiver) {
		this.receiver = receiver;
	}
	
	public boolean getState() {
		return state;
	}

	public void setState(boolean state) {
		this.state = state;
	}
	
	public boolean getNoificated() {
		return notificated;
	}

	public void setNotificated(boolean notificated) {
		this.notificated = notificated;
	}
	
	public boolean getSuggested() {
		return suggested;
	}

	public void setSuggested(boolean suggested) {
		this.suggested = suggested;
	}
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
	
}
