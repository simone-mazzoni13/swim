package swim.entitybeans;


import javax.persistence.*;

@Entity
@Table(name = "abilityresponse")
public class AbilityResponse {

	@Id
	@GeneratedValue
	@Column(name = "ID")	
	private long id;
	
	@OneToOne(targetEntity=swim.entitybeans.AbilityRequest.class)
	@JoinColumn(name = "id_ability_request")
	private AbilityRequest request;
	
	@ManyToOne(targetEntity=swim.entitybeans.Admin.class)
	@JoinColumn(name = "id_admin")
	private Admin admin;
	
	@Column(name = "response")
	private boolean response;

	@Column(name = "message", columnDefinition = "LONGTEXT")
	private String message;
	

	public long getId() {
		return id;
	}

	public AbilityRequest getRequest() {
		return request;
	}

	public void setRequest(AbilityRequest request) {
		this.request = request;
	}

	public Admin getAdmin() {
		return admin;
	}

	public void setAdmin(Admin admin) {
		this.admin = admin;
	}

	public boolean getResponse() {
		return response;
	}

	public void setResponse(boolean response) {
		this.response = response;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	
}