package swim.entitybeans;

import javax.persistence.*;

@Entity
@Table(name = "admin")
public class Admin {

	@Id
	@GeneratedValue
	@Column(name = "ID")	
	private long id;

	@Column(name = "username")	
	private String username;
	
	@Column(name = "password")	
	private String password;
	
	@Column(name = "email")	
	private String email;

	public long getId() {
		return id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
