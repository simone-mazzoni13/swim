package swim.entitybeans;


import java.util.Date;

import javax.persistence.*;

@Entity
@Table(name = "abilityrequest")
public class AbilityRequest {

	@Id
	@GeneratedValue
	@Column(name = "ID")	
	private long id;
	
	@ManyToOne(targetEntity=swim.entitybeans.RegisteredUser.class)
	@JoinColumn(name = "id_sender")
	private RegisteredUser sender;
	
	@Column(name = "state")
	private boolean state;
	
	@Column(name = "notificated")
	private boolean notificated;
	
	@Column(name = "message", columnDefinition = "LONGTEXT")
	private String message;
	
	@Column(name = "date")
	private Date date;

	public long getId() {
		return id;
	}

	public RegisteredUser getSender() {
		return sender;
	}

	public void setSender(RegisteredUser sender) {
		this.sender = sender;
	}

	public boolean getState() {
		return state;
	}

	public void setState(boolean state) {
		this.state = state;
	}

	public boolean isNotificated() {
		return notificated;
	}

	public void setNotificated(boolean notificated) {
		this.notificated = notificated;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
}
