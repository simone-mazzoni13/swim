package swim.sessionbeans;

import java.util.List;

import javax.ejb.Remote;

import swim.entitybeans.Ability;

@Remote
public interface AbilityManagerRemote {

	 public boolean addNewAbility(String name);
	 public List<Ability> listAllAbilities();	
	 
}
