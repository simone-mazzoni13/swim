package swim.sessionbeans;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.jboss.ejb3.annotation.RemoteBinding;

import swim.entitybeans.*;

/**
 * Session Bean implementation class FriendshipManager
 */
@Stateless
@RemoteBinding(jndiBinding="FriendshipManager/remote")
public class FriendshipManager implements FriendshipManagerRemote, FriendshipManagerLocal {

	@PersistenceContext(unitName="swim")
	private EntityManager em;
	
    /**
     * Default constructor. 
     */
    public FriendshipManager() {
        // TODO Auto-generated constructor stub
    }
    
	@SuppressWarnings("unchecked")
    public boolean addFriendship(long id1, long id2){
		try {
			RegisteredUser usr1 =  em.find(RegisteredUser.class, id1);
			RegisteredUser usr2 =  em.find(RegisteredUser.class, id2);
			if(!isFriend(usr1.getId(), usr2.getId())) {
				usr1.getFriends().add(usr2);
				usr2.getFriends().add(usr1);
				em.flush();
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;			
		}
		
		
		/*
		Query q1 = em.createQuery("FROM RegisteredUser u WHERE u.id = :user1");
		q1.setParameter("userId", id1);
		Query q2 = em.createQuery("FROM RegisteredUser u WHERE u.id = :user2");
		q2.setParameter("userId", id2);
		
		usr1= (RegisteredUser) q1.getSingleResult();
		usr2= (RegisteredUser) q2.getSingleResult();
		*/
		
		
		
    	
    }
	
	@SuppressWarnings("unchecked")
    public boolean isFriend(long user1, long user2) {
    	try {
    		Query q1 = em.createNativeQuery("SELECT * FROM friendship WHERE (id_user1 = :userId1 AND id_user2 = :userId2) OR (id_user1 = :userId2 AND id_user2 = :userId1)");
    		q1.setParameter("userId1", user1);
    		q1.setParameter("userId2", user2);
    		if (q1.getResultList().size() > 0) {
    			return true;
    		} else {
    			return false;
    		}	
    	} catch (Exception e) {
    		e.printStackTrace();
        	return false;
    	}
    }
    
    public boolean isFriendshipRequested(long userId1, long userId2) {
    	try {
    		Query q1 = em.createQuery("FROM FriendshipRequest WHERE ((sender = :user1 AND receiver = :user2) OR (receiver = :user1 AND sender = :user2)) AND state = false");
    		RegisteredUser user1 = em.find(RegisteredUser.class, userId1);
    		RegisteredUser user2 = em.find(RegisteredUser.class, userId2);    		
    		q1.setParameter("user1", user1);
    		q1.setParameter("user2", user2);
    		if (q1.getResultList().size() > 0) {
    			return true;
    		} else {
    			return false;
    		}	
    	} catch (Exception e) {
    		e.printStackTrace();
        	return false;
    	}
    }
    
    @SuppressWarnings("unchecked")
	public List<RegisteredUser> suggestFriends(long id_user, long id_otheruser){
		List<RegisteredUser> suggestions = new ArrayList<RegisteredUser>();
		try {
			Query q = em.createQuery("SELECT f FROM RegisteredUser u JOIN u.friends f WHERE u.id = :id_otheruser AND f.id <> :id_user AND f.id NOT IN (SELECT fd.id FROM RegisteredUser ud JOIN ud.friends fd WHERE  ud.id = :id_user)");
			q.setParameter("id_user", id_user);
			q.setParameter("id_otheruser", id_otheruser);
			suggestions.addAll(q.getResultList());
			return suggestions;			
		}
		catch (Exception ex) {
			ex.printStackTrace();
			return suggestions;
		}
	}

}
