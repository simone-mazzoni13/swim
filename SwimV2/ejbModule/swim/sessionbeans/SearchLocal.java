package swim.sessionbeans;

import java.util.List;
import java.util.Set;

import javax.ejb.Local;

import swim.entitybeans.RegisteredUser;

@Local
public interface SearchLocal {
	public Set<RegisteredUser> searchByUsername(String username);
	public Set<RegisteredUser> searchByMail(String mail);
}
