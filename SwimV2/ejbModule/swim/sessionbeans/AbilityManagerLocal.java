package swim.sessionbeans;

import java.util.List;

import javax.ejb.Local;

import swim.entitybeans.Ability;

@Local
public interface AbilityManagerLocal {

	 public boolean addNewAbility(String name);
	 
	 public List<Ability> listAllAbilities();
}
