package swim.sessionbeans;

import java.util.List;

import javax.ejb.Local;

import swim.entitybeans.RegisteredUser;

@Local
public interface FriendshipManagerLocal {
	
    public boolean addFriendship(long user1, long user2);
    
    public boolean isFriend(long user1, long user2);
    
    public boolean isFriendshipRequested(long user1, long user2);
    
    public List<RegisteredUser> suggestFriends(long id_user, long id_otheruser);

}
